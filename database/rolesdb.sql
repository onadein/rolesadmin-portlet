create role rolesdb;
alter role rolesdb with nosuperuser inherit nocreaterole nocreatedb login password 'q1w2e3' valid until 'infinity';

create database rolesdb owner = rolesdb;
\connect rolesdb

--///////////////////////////////////////////////////////////////////////////////////////////////////
--// Table: commands
--//
create table commands
(
  CID serial NOT NULL,                              -- Уникальный идентификатор
  name character varying NOT NULL,                  -- Название команды
  description character varying,                    -- Описание команды

  constraint pk_commands_cid primary key (CID),
  constraint uniq_commands_name unique (name)
)
with (
  OIDS=FALSE
);
alter table commands owner to rolesdb;


--///////////////////////////////////////////////////////////////////////////////////////////////////
--// Table: groups
--//
create table groups
(
  GID serial NOT NULL,                              -- Уникальный идентификатор
  name character varying NOT NULL,                  -- Название группы
  description character varying,                    -- Описание группы

  constraint pk_groups_cid primary key (GID),
  constraint uniq_groups_name unique (name)
)
with (
  OIDS=FALSE
);
alter table groups owner to rolesdb;

--///////////////////////////////////////////////////////////////////////////////////////////////////
--// Table: groups_commands
--//
create table groups_commands
(
  GID integer NOT NULL,                             -- Уникальный идентификатор группы
  CID integer NOT NULL,                             -- Уникальный идентификатор команды

  constraint pk_groups_commands primary key (GID, CID),
  constraint fk_groups_commands_commands foreign key (CID) references commands (CID) match simple on update no action on delete no action,
  constraint fk_groups_commands_roles foreign key (GID) references groups (GID) match simple on update no action on delete no action
)
with (
  OIDS=FALSE
);
alter table groups_commands owner to rolesdb;

create index fki_groups_commands_commands on groups_commands using btree (CID);


--///////////////////////////////////////////////////////////////////////////////////////////////////
--// Table: roles
--//
create table roles
(
  RID serial NOT NULL,                              -- Уникальный идентификатор роли
  name character varying,                           -- Название роли
  description character varying,                    -- Описание роли
  last_update timestamp without time zone DEFAULT now(),

  constraint pk_rid primary key(RID),
  constraint uniq_name unique(name)
)
with (
  OIDS=FALSE
);
alter table roles owner to rolesdb;

--///////////////////////////////////////////////////////////////////////////////////////////////////
--// Table: roles_groups
--//

create table roles_groups
(
  RID integer NOT NULL,                             -- Уникальный идентификатор роли
  GID integer NOT NULL,                             -- Уникальный идентификатор команды

  constraint pk_roles_groups primary key (RID, GID),
  constraint fk_roles_groups_commands foreign key (GID) references groups (GID) match simple on update no action on delete no action,
  constraint fk_roles_groups_roles foreign key (RID) references roles (RID) match simple on update no action on delete no action
)
with (
  OIDS=FALSE
);
alter table roles_groups owner to rolesdb;

create index fki_roles_groups_commands on roles_groups using btree (GID);


--///////////////////////////////////////////////////////////////////////////////////////////////////
--// Table: whitelist
--//
CREATE TABLE whitelist
(
  id serial NOT NULL,
  netaddress text,
  netname text,
  netrole integer,
  last_update timestamp without time zone DEFAULT now(),
  CONSTRAINT pk_whitelist_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE whitelist
  OWNER TO rolesdb;

ALTER TABLE whitelist
  ADD CONSTRAINT fk_whitelist_roles FOREIGN KEY (netrole) REFERENCES roles (rid)
   ON UPDATE NO ACTION ON DELETE NO ACTION;
CREATE INDEX fki_whitelist_roles
  ON whitelist(netrole);

--///////////////////////////////////////////////////////////////////////////////
--// Функция и триггеры для обновления даты/времени в записях
--//

CREATE OR REPLACE FUNCTION make_plpgsql()
RETURNS VOID
LANGUAGE SQL
AS $$
CREATE LANGUAGE plpgsql;
$$;
 
SELECT
    CASE
    WHEN EXISTS(
        SELECT 1
        FROM pg_catalog.pg_language
        WHERE lanname='plpgsql'
    )
    THEN NULL
    ELSE make_plpgsql() END;
 
DROP FUNCTION make_plpgsql();
  
CREATE OR REPLACE FUNCTION update_lastupdate_column()	
RETURNS TRIGGER AS $$
BEGIN
    NEW.last_update = now();
    RETURN NEW;	
END;
$$ language 'plpgsql';

CREATE TRIGGER update_whitelist_lastupdate BEFORE UPDATE ON whitelist FOR EACH ROW EXECUTE PROCEDURE update_lastupdate_column();
CREATE TRIGGER update_roles_lastupdate BEFORE UPDATE ON roles FOR EACH ROW EXECUTE PROCEDURE update_lastupdate_column();

--///////////////////////////////////////////////////////////////////////////////////////////////////
--// Заполнение таблиц
--//

--///////////////////////////////////////////////////////////////////////////////////////////////////
--// commands

insert into commands (CID, name, description) values (      301,                    'AddData',                       'Добавление данных');
insert into commands (CID, name, description) values (      201,                    'GetDataInfo',                   'Получение информации');
insert into commands (CID, name, description) values (      202,                    'GetData',                       'Выгрузка данных');
insert into commands (CID, name, description) values (      401,                    'DeleteData',                    'Удаление данных');
insert into commands (CID, name, description) values (      402,                    'RestoreData',                   'Восстановление данных');


--///////////////////////////////////////////////////////////////////////////////////////////////////
--// groups
insert into groups (GID, name, description) values (2100, 'Общая информация', 'Общая информация');
insert into groups (GID, name, description) values (2200, 'Чтение данных', 'Чтение и выгрузка данных');
insert into groups (GID, name, description) values (2300, 'Добавление данных', 'Изменение данных');
insert into groups (GID, name, description) values (2400, 'Удаление данных', 'Удаление данных');

--///////////////////////////////////////////////////////////////////////////////////////////////////
--// roles
insert into roles values (1001, 'Администрирование', 'Выполнение административных функций');
insert into roles values (1002, 'Полный доступ', 'Полный доступ к административному и функциональному интерфейсам');

--///////////////////////////////////////////////////////////////////////////////////////////////////
--// whitelisted networks
INSERT INTO whitelist (netaddress, netname, netrole) VALUES ('0.0.0.0/0', 'Полный доступ для всех', 1002);

--///////////////////////////////////////////////////////////////////////////////////////////////////
--// groups_commands
--//Чтение данных
insert into groups_commands values (2200, 201);
insert into groups_commands values (2200, 202);

--//Добавление данных
insert into groups_commands values (2300, 301);

--//Удаление данных
insert into groups_commands values (2400, 401);
insert into groups_commands values (2400, 402);


--///////////////////////////////////////////////////////////////////////////////////////////////////
--// roles_groups

--//  Администрирование
insert into roles_groups values (1001, 2100);

--// Полный доступ
insert into roles_groups values (1002, 2100);
insert into roles_groups values (1002, 2200);
insert into roles_groups values (1002, 2300);
insert into roles_groups values (1002, 2400);

