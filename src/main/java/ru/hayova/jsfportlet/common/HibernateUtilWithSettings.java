package ru.hayova.jsfportlet.common;

//(c) Oleg 'hayova' Nadein, 2016

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.faces.context.FacesContext;
import javax.portlet.PortletRequest;

public class HibernateUtilWithSettings {
	private static SessionFactory sessionFactory;
	static {
		try {
			//sessionFactory = new Configuration().configure(BdbDefs.HIBERNATE_CONFIG_NAME).buildSessionFactory();

			//	ON: если вдруг будем настраивать юзера-пароли-пути в настройках портлета:			
			Configuration cfg = new Configuration();
			cfg.configure(DbDefs.HIBERNATE_CONFIG_NAME);

			String defaultConnectionUserPref = ((PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getPreferences().getValue(DbDefs.PORTLET_PREF_DB_USERNAME, DbDefs.PORTLET_PREF_VALUE_DB_DEFAULT_USER);
			if(defaultConnectionUserPref != null && !defaultConnectionUserPref.isEmpty())
				System.setProperty(DbDefs.PORTLET_HIBERNATE_PROPERTY_USERNAME, defaultConnectionUserPref);
			
			String defaultConnectionPassPref = ((PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getPreferences().getValue(DbDefs.PORTLET_PREF_DB_PASSWORD, DbDefs.PORTLET_PREF_VALUE_DB_DEFAULT_PASSOWRD);
			if(defaultConnectionPassPref != null && !defaultConnectionPassPref.isEmpty())
				System.setProperty(DbDefs.PORTLET_HIBERNATE_PROPERTY_PASSWORD, defaultConnectionPassPref);

			String connString = DbDefs.PORTLET_PREF_DB_CONN_PROTOCOL + DbDefs.PORTLET_PREF_VALUE_DB_DEFAULT_PATH + "/" + DbDefs.PORTLET_PREF_VALUE_DB_DEFAULT_DATABASE;
			
			String defaultConnectionDBNamePref = ((PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getPreferences().getValue(DbDefs.PORTLET_PREF_DB_NAME, DbDefs.PORTLET_PREF_VALUE_DB_DEFAULT_DATABASE);
			String defaultConnectionDBPathPref = ((PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getPreferences().getValue(DbDefs.PORTLET_PREF_DB_PATH, DbDefs.PORTLET_PREF_VALUE_DB_DEFAULT_PATH);			
			if(defaultConnectionDBNamePref != null && !defaultConnectionDBNamePref.isEmpty())
				if(defaultConnectionDBNamePref != null && !defaultConnectionDBNamePref.isEmpty())				
					connString = DbDefs.PORTLET_PREF_DB_CONN_PROTOCOL + defaultConnectionDBPathPref +  "/" + defaultConnectionDBNamePref;
			if(connString != null && !connString.isEmpty())
				System.setProperty(DbDefs.PORTLET_HIBERNATE_PROPERTY_URL, connString);

			cfg.setProperties(System.getProperties());
			sessionFactory = cfg.buildSessionFactory(); 			
			
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}