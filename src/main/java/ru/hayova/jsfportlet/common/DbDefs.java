package ru.hayova.jsfportlet.common;

//(c) Oleg 'hayova' Nadein, 2016

import java.io.Serializable;

public class DbDefs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -950737451799576439L;
	
	public static final Integer DEFAULT_ROWS_PER_PAGE_COUNT = 5;
	
	public static final String HIBERNATE_CONFIG_NAME = "hibernate.cfg.xml";
	
	public static final String PORTLET_PREF_DB_CONN_PROTOCOL = "jdbc:postgresql://";
	
	public static final String PORTLET_PREF_VALUE_DB_DEFAULT_USER = "rolesdb";
	public static final String PORTLET_PREF_VALUE_DB_DEFAULT_PASSOWRD = "q1w2e3";
	public static final String PORTLET_PREF_VALUE_DB_DEFAULT_DATABASE = "rolesdb";
	public static final String PORTLET_PREF_VALUE_DB_DEFAULT_PATH = "localhost:5432";
	
	public static final String PORTLET_PREF_DB_USERNAME = "defaultUsername";
	public static final String PORTLET_PREF_DB_PASSWORD = "defaultPassword";
	public static final String PORTLET_PREF_DB_NAME = "defaultDBName";
	public static final String PORTLET_PREF_DB_PATH = "defaultDBPath";
	
	public static final String PORTLET_HIBERNATE_PROPERTY_USERNAME = "hibernate.connection.username";
	public static final String PORTLET_HIBERNATE_PROPERTY_PASSWORD = "hibernate.connection.password";
	public static final String PORTLET_HIBERNATE_PROPERTY_URL = "hibernate.connection.url";
	
}
