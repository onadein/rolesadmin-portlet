package ru.hayova.jsfportlet.common;

//(c) Oleg 'hayova' Nadein, 2016

import java.io.Serializable;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

import ru.hayova.jsfportlet.rolesdb.Roles;

@ManagedBean(name = "bdbUtils")
public class DbUtils implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1057886921957074931L;

	public static String muiString(String key) {
        try {
        	// ON: Удалить new UTF8Control() если properties в ASCII (не для Liferay)
            return ResourceBundle.getBundle("Language", FacesContext.getCurrentInstance().getExternalContext().getRequestLocale(), new UTF8Control()).getString(key);
        } catch (MissingResourceException e) {
            return key;
        }
	}
	
	public static String testConnectionToDB() {
		String result = null;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			Criteria criteria = session.createCriteria(Roles.class);
			criteria.setProjection(Projections.rowCount()).uniqueResult();
		} catch (HibernateException e) {
			result = e.getMessage();
			e.printStackTrace();
		} finally {
			session.close();
		}		
		
		return result;
	}
	
	public static Boolean compareTimestamps(Date currentTimestamp, Date lastTimestamp) {
		Boolean result = false;
		
		if(currentTimestamp != null && lastTimestamp != null) {
			if(currentTimestamp.getTime() == lastTimestamp.getTime()) {
				result = true;
			}
		}
		
		return result;
		
	}
	
}
