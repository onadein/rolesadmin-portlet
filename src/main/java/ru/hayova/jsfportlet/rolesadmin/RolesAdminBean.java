package ru.hayova.jsfportlet.rolesadmin;

//(c) Oleg 'hayova' Nadein, 2016

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.primefaces.context.RequestContext;
import org.primefaces.event.data.FilterEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.LazyDataModel;

import ru.hayova.jsfportlet.rolesdb.Commands;
import ru.hayova.jsfportlet.rolesdb.Groups;
import ru.hayova.jsfportlet.rolesdb.Roles;
import ru.hayova.jsfportlet.rolesdb.Whitelist;
import ru.hayova.jsfportlet.common.DbUtils;
import ru.hayova.jsfportlet.common.HibernateUtil;


@ManagedBean(name = "rolesAdminBean")
@ViewScoped
public class RolesAdminBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private LazyDataModel<Whitelist> lazyWhitelistModel;
	private List<Whitelist> filteredWhitelist;
	private List<Whitelist> selectedWhitelists;

	private LazyDataModel<Commands> lazyCommandsModel;
	private List<Commands> filteredCommands;
	private List<Commands> selectedCommands;

	private LazyDataModel<Roles> lazyRolesModel;
	private List<Roles> filteredRoles;
	private List<Roles> selectedRoles;
	
	private LazyDataModel<Groups> lazyGroupsModel;
	
	private Integer editedWhitelistId = -1;
	private String editedWhitelistAddress = "";
	private String editedWhitelistName = "";
	private String editedWhitelistRole = "";
	private Date editedWhitelistLastUpdate;

	private SelectItem[] rolesOptions;
	private SelectItem[] rolesList;

	private String editedCommandId = "";
	private String editedCommandName = "";
	private String editedCommandDesc = "";

	private String editedRoleId = "";
	private String editedRoleName = "";
	private String editedRoleDesc = "";
	private Date editedRoleLastUpdate;
	
	private boolean editMark;
	
	private DualListModel<Groups> groupsForRole;
	
	private boolean connectionToDb;
	
	public RolesAdminBean(){
		String lastDBError = DbUtils.testConnectionToDB();
		connectionToDb = (lastDBError == null ? true : false);
		
		lazyWhitelistModel = new LazyWhitelistModel();
		lazyCommandsModel = new LazyCommandsModel();
		lazyRolesModel = new LazyRolesModel(); 
		lazyGroupsModel = new LazyGroupsModel();
		
		groupsForRole = new DualListModel<Groups>(new ArrayList<Groups>(), new ArrayList<Groups>());
		
		fillRolesOptions();
	}

	public String commandsOfGroup(Groups group){
		String result = "";

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Commands.class);
			criteria.setFetchMode("groupses", FetchMode.JOIN);
			criteria.createAlias("groupses", "gs");
			criteria.add(Restrictions.eq("gs.gid", group.getGid()));
			@SuppressWarnings("unchecked")
			List<Commands> allCommandsList = criteria.list();
			if(allCommandsList != null) {
				for(Commands command : allCommandsList) {
					if(result.length() > 1)
						result += ", " + command.getName();
					else
						result += command.getName();
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}
		
		return result;
	}
	
	private boolean isCommandInCurrentCommandsList(Integer commandId) {
		for(Groups curCol : groupsForRole.getTarget()) {
			if(curCol.getGid() == commandId)
				return true;
		}
		
		return false;
	}	
	
	private void refillCommandsPickList() {
		groupsForRole.getSource().clear();
		groupsForRole.getTarget().clear();

		if((editedRoleId != null) && (!editedRoleId.isEmpty())) {
			Session session = HibernateUtil.getSessionFactory().openSession();
			try {
				session.beginTransaction();
				Roles role = (Roles) session.get(Roles.class, Integer.valueOf(editedRoleId));
				if(role != null) {
					if (role.getGroupses() != null) {
						for(Groups group : role.getGroupses()) {
							groupsForRole.getTarget().add(group);
							
						}
					}
				}
				session.getTransaction().commit();
			} catch (HibernateException e) {
				if(session.getTransaction()!=null)
					session.getTransaction().rollback();
				e.printStackTrace();
			} finally {
				session.flush();session.close();
			}		
		}	
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Criteria criteria=session.createCriteria(Groups.class);
			@SuppressWarnings("unchecked")
			List<Groups> allGroupsList = criteria.list();
			if(allGroupsList != null) {
				for(Groups group : allGroupsList) {
					if(!isCommandInCurrentCommandsList(group.getGid()))
						groupsForRole.getSource().add(group);
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}		
	}
	
	private void fillRolesOptions(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Criteria criteria=session.createCriteria(Roles.class);
			criteria.addOrder(Order.asc("rid"));
			@SuppressWarnings("unchecked")
			List<Roles> rowList = (List<Roles>) criteria.list();
			if(rowList != null){
				rolesOptions = new SelectItem[rowList.size() + 1];
				rolesList = new SelectItem[rowList.size()];
				rolesOptions[0] = new SelectItem("", DbUtils.muiString("all-records"));
				int i=1;
				for (Iterator<Roles> it = rowList.iterator(); it.hasNext();) {
					Roles role = it.next();
					rolesList[i-1] = new SelectItem(role.getRid(), role.getName());
					rolesOptions[i++] = new SelectItem(role.getRid(), role.getName());
				}
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}				
	}
	
	public void showWhitelistRecord(Whitelist selectedWhitelist) {
		editMark = true;
		editedWhitelistId = selectedWhitelist.getId();
		editedWhitelistAddress = selectedWhitelist.getNetaddress();
		editedWhitelistName = selectedWhitelist.getNetname();
		editedWhitelistRole = String.valueOf(selectedWhitelist.getRoles().getRid());
		editedWhitelistLastUpdate = selectedWhitelist.getLastUpdate();
	}

	public void addNetwork() {
		editMark = false;
		editedWhitelistAddress = "";
		editedWhitelistName = "";
		editedWhitelistRole = String.valueOf(rolesList[0].getValue());
	}
	
	public void showRolesRecord(Roles selectedRole) {
		editMark = true;
		editedRoleId = String.valueOf(selectedRole.getRid());
		editedRoleName = selectedRole.getName();
		editedRoleDesc = selectedRole.getDescription();
		editedRoleLastUpdate = selectedRole.getLastUpdate();
		refillCommandsPickList();
	}	
	
	private boolean checkForRoleId(Integer roleId){
		boolean result = false;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Roles role = (Roles) session.get(Roles.class, roleId);
			if(role != null)
				result = true;
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}			
		
		return result;
	}
	
	private boolean checkForCommandId(Integer commandId){
		boolean result = false;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Commands command = (Commands) session.get(Commands.class, commandId);
			if(command != null)
				result = true;
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}			
		
		return result;
	}

	public void saveWhitelist() {
		if(!editMark) {
			saveNewWhitelist();
			RequestContext.getCurrentInstance().execute("PF('editNetworkDialog').hide();");
		} else {
			if(editWhitelist(editedWhitelistLastUpdate))
				RequestContext.getCurrentInstance().execute("PF('editNetworkDialog').hide();");
			else
				showRecordWasChangedError();	
		}
	}
	
	private void showRecordWasChangedError() {
		FacesContext.getCurrentInstance().addMessage("growlmessage",
				new FacesMessage(FacesMessage.SEVERITY_ERROR, DbUtils.muiString("record-was-changed"), ""));
	}
	
	public void saveRole() {
		if(!editMark) {
			if(!checkForRoleId(Integer.parseInt(editedRoleId))) {
				saveNewRole();
				RequestContext.getCurrentInstance().execute("PF('editRoleDialog').hide();");
			} else {
				FacesContext.getCurrentInstance().addMessage("growlmessage",
						new FacesMessage(FacesMessage.SEVERITY_WARN, DbUtils.muiString("record-with-given-id-exists"), ""));
			}
		} else {
			if(editRole(editedRoleLastUpdate))
				RequestContext.getCurrentInstance().execute("PF('editRoleDialog').hide();");
			else
				showRecordWasChangedError();
		}		
	}

	public void saveCommand() {
		if(!editMark) {
			if(!checkForCommandId(Integer.parseInt(editedCommandId))) {
				saveNewCommand();
				RequestContext.getCurrentInstance().execute("PF('editCommandDialog').hide();");
			} else {
				FacesContext.getCurrentInstance().addMessage("growlmessage",
						new FacesMessage(FacesMessage.SEVERITY_WARN, DbUtils.muiString("record-with-given-id-exists"), ""));
			}
		} else {
			editCommand();
			RequestContext.getCurrentInstance().execute("PF('editCommandDialog').hide();");
		}
	}
	
	private void saveNewWhitelist() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Whitelist wl = new Whitelist();
			wl.setNetaddress(editedWhitelistAddress);
			wl.setNetname(editedWhitelistName);
			wl.setRoles(new Roles(Integer.parseInt(editedWhitelistRole)));
			session.save(wl);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}
	}
	
	private void saveNewRole() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Roles role = new Roles();
			role.setRid(Integer.parseInt(editedRoleId));
			role.setName(editedRoleName);
			role.setDescription(editedRoleDesc);
			role.setGroupses(new HashSet<Groups>(groupsForRole.getTarget()));
			session.save(role);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}		
	}
	
	private void saveNewCommand() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Commands command = new Commands();
			command.setCid(Integer.parseInt(editedCommandId));
			command.setName(editedCommandName);
			command.setDescription(editedCommandDesc);
			session.save(command);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}		
	}
	
	private Boolean editRole(Date lastTimestamp) {
		Boolean result = false;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Roles role = (Roles) session.get(Roles.class, Integer.parseInt(editedRoleId), LockOptions.UPGRADE);
			if(DbUtils.compareTimestamps(role.getLastUpdate(), lastTimestamp)) {
				role.setName(editedRoleName);
				role.setDescription(editedRoleDesc);
				role.setGroupses(new HashSet<Groups>(groupsForRole.getTarget()));
				session.update(role);
				result = true;
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		
		return result;
	}

	private Boolean editWhitelist(Date lastTimestamp) {
		Boolean result = false;
		
		if(editedWhitelistId >= 0) {
			Session session = HibernateUtil.getSessionFactory().openSession();
			try {
				session.beginTransaction();
				Whitelist wl = (Whitelist) session.get(Whitelist.class, editedWhitelistId, LockOptions.UPGRADE);
				if(DbUtils.compareTimestamps(wl.getLastUpdate(), lastTimestamp)) {
					wl.setNetaddress(editedWhitelistAddress);
					wl.setNetname(editedWhitelistName);
					wl.setRoles(new Roles(Integer.parseInt(editedWhitelistRole)));
					session.update(wl);
					result = true;
				}
				session.getTransaction().commit();
			} catch (HibernateException e) {
				if(session.getTransaction()!=null)
					session.getTransaction().rollback();
				e.printStackTrace();
			} finally {
				session.flush();session.close();
			}
		}
		
		return result;
	}
	
	private void editCommand() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Commands command = (Commands) session.get(Commands.class, Integer.parseInt(editedCommandId));
			command.setName(editedCommandName);
			command.setDescription(editedCommandDesc);
			session.update(command);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}			
	}
	
	public void addRole() {
		editMark = false;
		editedRoleId = "";
		editedRoleName = "";
		editedRoleDesc = "";
		refillCommandsPickList();
	}
	
	private static String deleteWhitelistRecords(List<Whitelist> whitelistsToDelete){
		String result = null;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			for (Whitelist whitelist : whitelistsToDelete) {
				Whitelist wl = (Whitelist) session.get(Whitelist.class, whitelist.getId());
				session.delete(wl);
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			result = e.getClass().getSimpleName();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}
		
		return result;
	}
	
	private static String deleteRolesRecords(List<Roles> rolesToDelete){
		String result = null;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			for (Roles role : rolesToDelete) {
				Roles wl = (Roles) session.get(Roles.class, role.getRid());
				session.delete(wl);
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			result = e.getClass().getSimpleName();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}
		
		return result;
	}	
	
	private static String deleteCommandsRecords(List<Commands> commandsToDelete){
		String result = null;
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			for (Commands command : commandsToDelete) {
				Commands wl = (Commands) session.get(Commands.class, command.getCid());
				session.delete(wl);
			}
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if(session.getTransaction()!=null)
				session.getTransaction().rollback();
			result = e.getClass().getSimpleName();
			e.printStackTrace();
		} finally {
			session.flush();session.close();
		}
		
		return result;
	}

	public void deleteNetworks() {
		String errRes = deleteWhitelistRecords(selectedWhitelists);
		if(errRes != null && !errRes.isEmpty())
			FacesContext.getCurrentInstance().addMessage("growlmessage", new FacesMessage(FacesMessage.SEVERITY_ERROR, DbUtils.muiString("some-records-not-deleted"), DbUtils.muiString("reason") + ": " + errRes));
		else
			FacesContext.getCurrentInstance().addMessage("growlmessage", new FacesMessage(FacesMessage.SEVERITY_INFO, DbUtils.muiString("records-successfully-deleted"), ""));
	}
	
	public void deleteRoles() {
		String errRes = deleteRolesRecords(selectedRoles);
		if(errRes != null && !errRes.isEmpty())
			FacesContext.getCurrentInstance().addMessage("growlmessage", new FacesMessage(FacesMessage.SEVERITY_ERROR, DbUtils.muiString("some-records-not-deleted"), DbUtils.muiString("reason") + ": " + errRes));
		else
			FacesContext.getCurrentInstance().addMessage("growlmessage", new FacesMessage(FacesMessage.SEVERITY_INFO, DbUtils.muiString("records-successfully-deleted"), ""));
	}
	
	public void deleteCommands() {
		String errRes = deleteCommandsRecords(selectedCommands);
		if(errRes != null && !errRes.isEmpty())
			FacesContext.getCurrentInstance().addMessage("growlmessage", new FacesMessage(FacesMessage.SEVERITY_ERROR, DbUtils.muiString("some-records-not-deleted"), DbUtils.muiString("reason") + ": " + errRes));
		else
			FacesContext.getCurrentInstance().addMessage("growlmessage", new FacesMessage(FacesMessage.SEVERITY_INFO, DbUtils.muiString("records-successfully-deleted"), ""));
	}

	public void testDeleteNetworks() {
		if (selectedWhitelists != null && !selectedWhitelists.isEmpty()) {
			RequestContext.getCurrentInstance().execute("PF('confirmRemoveNetworksDialog').show();");
		} else
			FacesContext.getCurrentInstance().addMessage("growlmessage",
					new FacesMessage(FacesMessage.SEVERITY_WARN, DbUtils.muiString("select-records-to-delete"), ""));
	}
	
	public void testDeleteRoles() {
		if (selectedRoles != null && !selectedRoles.isEmpty()) {
			RequestContext.getCurrentInstance().execute("PF('confirmRemoveRolesDialog').show();");
		} else
			FacesContext.getCurrentInstance().addMessage("growlmessage",
					new FacesMessage(FacesMessage.SEVERITY_WARN, DbUtils.muiString("select-records-to-delete"), ""));
	}
	
	public void testDeleteCommands() {
		if (selectedCommands != null && !selectedCommands.isEmpty()) {
			RequestContext.getCurrentInstance().execute("PF('confirmRemoveCommandsDialog').show();");
		} else
			FacesContext.getCurrentInstance().addMessage("growlmessage",
					new FacesMessage(FacesMessage.SEVERITY_WARN, DbUtils.muiString("select-records-to-delete"), ""));
	}
	
	public void listenFilterWhitelist(FilterEvent event) {		  
		((LazyWhitelistModel)lazyWhitelistModel).setCurrentFilters(event.getFilters());
	}

	public void listenFilterRoles(FilterEvent event) {		  
		((LazyRolesModel)lazyRolesModel).setCurrentFilters(event.getFilters());
	}

	public void listenFilterCommands(FilterEvent event) {		  
		((LazyCommandsModel)lazyCommandsModel).setCurrentFilters(event.getFilters());
	}

	
	
	public LazyDataModel<Whitelist> getLazyWhitelistModel() {
		return lazyWhitelistModel;
	}

	public void setLazyWhitelistModel(LazyDataModel<Whitelist> lazyWhitelistModel) {
		this.lazyWhitelistModel = lazyWhitelistModel;
	}

	public List<Whitelist> getFilteredWhitelist() {
		return filteredWhitelist;
	}

	public void setFilteredWhitelist(List<Whitelist> filteredWhitelist) {
		this.filteredWhitelist = filteredWhitelist;
	}

	public List<Whitelist> getSelectedWhitelists() {
		return selectedWhitelists;
	}

	public void setSelectedWhitelists(List<Whitelist> selectedWhitelists) {
		this.selectedWhitelists = selectedWhitelists;
	}

	public SelectItem[] getRolesOptions() {
		return rolesOptions;
	}

	public void setRolesOptions(SelectItem[] rolesOptions) {
		this.rolesOptions = rolesOptions;
	}

	public SelectItem[] getRolesList() {
		return rolesList;
	}

	public void setRolesList(SelectItem[] rolesList) {
		this.rolesList = rolesList;
	}

	public String getEditedWhitelistAddress() {
		return editedWhitelistAddress;
	}

	public void setEditedWhitelistAddress(String editedWhitelistAddress) {
		this.editedWhitelistAddress = editedWhitelistAddress;
	}

	public String getEditedWhitelistName() {
		return editedWhitelistName;
	}

	public void setEditedWhitelistName(String editedWhitelistName) {
		this.editedWhitelistName = editedWhitelistName;
	}

	public String getEditedWhitelistRole() {
		return editedWhitelistRole;
	}

	public void setEditedWhitelistRole(String editedWhitelistRole) {
		this.editedWhitelistRole = editedWhitelistRole;
	}

	public boolean isEditMark() {
		return editMark;
	}

	public void setEditMark(boolean editMark) {
		this.editMark = editMark;
	}

	public Integer getEditedWhitelistId() {
		return editedWhitelistId;
	}

	public void setEditedWhitelistId(Integer editedWhitelistId) {
		this.editedWhitelistId = editedWhitelistId;
	}

	public LazyDataModel<Commands> getLazyCommandsModel() {
		return lazyCommandsModel;
	}

	public void setLazyCommandsModel(LazyDataModel<Commands> lazyCommandsModel) {
		this.lazyCommandsModel = lazyCommandsModel;
	}

	public List<Commands> getFilteredCommands() {
		return filteredCommands;
	}

	public void setFilteredCommands(List<Commands> filteredCommands) {
		this.filteredCommands = filteredCommands;
	}

	public List<Commands> getSelectedCommands() {
		return selectedCommands;
	}

	public void setSelectedCommands(List<Commands> selectedCommands) {
		this.selectedCommands = selectedCommands;
	}

	public String getEditedCommandName() {
		return editedCommandName;
	}

	public void setEditedCommandName(String editedCommandName) {
		this.editedCommandName = editedCommandName;
	}

	public String getEditedCommandDesc() {
		return editedCommandDesc;
	}

	public void setEditedCommandDesc(String editedCommandDesc) {
		this.editedCommandDesc = editedCommandDesc;
	}

	public String getEditedCommandId() {
		return editedCommandId;
	}

	public void setEditedCommandId(String editedCommandId) {
		this.editedCommandId = editedCommandId;
	}

	public String getEditedRoleId() {
		return editedRoleId;
	}

	public void setEditedRoleId(String editedRoleId) {
		this.editedRoleId = editedRoleId;
	}

	public String getEditedRoleName() {
		return editedRoleName;
	}

	public void setEditedRoleName(String editedRoleName) {
		this.editedRoleName = editedRoleName;
	}

	public String getEditedRoleDesc() {
		return editedRoleDesc;
	}

	public void setEditedRoleDesc(String editedRoleDesc) {
		this.editedRoleDesc = editedRoleDesc;
	}

	public LazyDataModel<Roles> getLazyRolesModel() {
		return lazyRolesModel;
	}

	public void setLazyRolesModel(LazyDataModel<Roles> lazyRolesModel) {
		this.lazyRolesModel = lazyRolesModel;
	}

	public List<Roles> getFilteredRoles() {
		return filteredRoles;
	}

	public void setFilteredRoles(List<Roles> filteredRoles) {
		this.filteredRoles = filteredRoles;
	}

	public List<Roles> getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(List<Roles> selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	public boolean isConnectionToDb() {
		return connectionToDb;
	}

	public void setConnectionToDb(boolean connectionToDb) {
		this.connectionToDb = connectionToDb;
	}

	public DualListModel<Groups> getGroupsForRole() {
		return groupsForRole;
	}

	public void setGroupsForRole(DualListModel<Groups> groupsForRole) {
		this.groupsForRole = groupsForRole;
	}

	public LazyDataModel<Groups> getLazyGroupsModel() {
		return lazyGroupsModel;
	}

	public void setLazyGroupsModel(LazyDataModel<Groups> lazyGroupsModel) {
		this.lazyGroupsModel = lazyGroupsModel;
	}

	public Date getEditedRoleLastUpdate() {
		return editedRoleLastUpdate;
	}

	public void setEditedRoleLastUpdate(Date editedRoleLastUpdate) {
		this.editedRoleLastUpdate = editedRoleLastUpdate;
	}

	public Date getEditedWhitelistLastUpdate() {
		return editedWhitelistLastUpdate;
	}

	public void setEditedWhitelistLastUpdate(Date editedWhitelistLastUpdate) {
		this.editedWhitelistLastUpdate = editedWhitelistLastUpdate;
	}


	
}
