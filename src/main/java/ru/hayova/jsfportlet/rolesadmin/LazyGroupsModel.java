package ru.hayova.jsfportlet.rolesadmin;

//(c) Oleg 'hayova' Nadein, 2016

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import ru.hayova.jsfportlet.rolesdb.Groups;
import ru.hayova.jsfportlet.common.HibernateUtil;

public class LazyGroupsModel extends LazyDataModel<Groups>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8847430327936711947L;
	
	private Map<String,Object> currentFilters;

	public LazyGroupsModel() {
		currentFilters = null;
		setCount(null);
	}

	private void setCount(Map<String,Object> filters){
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			Criteria criteria=session.createCriteria(Groups.class);
			if(filters!=null)
				createFilter(criteria, filters, null,null);					
			long count=(long)criteria.setProjection(Projections.rowCount()).uniqueResult();
			this.setRowCount((int) count);
		} catch (HibernateException e) {
			e.printStackTrace();
		}catch (Exception e) {
			System.err.println(e.getMessage());
		} 
		finally {
			session.close();
		}
	}

	private void createFilter(Criteria criteria,Map<String,Object> filters,String sortField, SortOrder sortOrder)
	{
		Map<String,Criteria> subCrit=new HashMap<String,Criteria>();

		if(filters!=null)
			for(Entry<String, Object> entry : filters.entrySet()) {
				if(entry.getKey().contains("."))
				{
					String className=entry.getKey().split("\\.")[0];
					String fieldName=entry.getKey().split("\\.")[1];
					if(!subCrit.containsKey(className))
					{
						subCrit.put(className, criteria.createCriteria(className));
					}
					subCrit.get(className).add(Restrictions.ilike(fieldName, "%"+entry.getValue()+"%"));
				}else {
					if(entry.getKey().equals("gid"))
						criteria.add(Restrictions.eq(entry.getKey(), Integer.parseInt((String) entry.getValue())));
					else
						criteria.add(Restrictions.ilike(entry.getKey(), "%"+entry.getValue()+"%"));
				}
			}

		if(sortField!=null){
			if(sortField.contains("."))
			{
				String className=sortField.split("\\.")[0];
				String fieldName=sortField.split("\\.")[1];
				if(!subCrit.containsKey(className))
				{
					subCrit.put(className, criteria.createCriteria(className));
				}
				subCrit.get(className).addOrder(sortOrder.equals(SortOrder.ASCENDING)?Order.asc(fieldName):Order.desc(fieldName));
			}
			else
				criteria.addOrder(sortOrder.equals(SortOrder.ASCENDING)?Order.asc(sortField):Order.desc(sortField));
		}		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Groups> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
		setCount(currentFilters);
		List<Groups> requestList=null;

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			Criteria criteria=session.createCriteria(Groups.class);

			criteria.setFirstResult(first);
			criteria.setMaxResults(pageSize);
			//criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			//criteria.setProjection(Projections.distinct(Projections.property("gid")));			
			//criteria.setFetchMode("commandses", FetchMode.JOIN);
			
			createFilter(criteria, currentFilters, sortField, sortOrder);

			requestList=criteria.list();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return requestList;
	}
	
	@Override
	public Object getRowKey(Groups role) {
	    return role != null ? role.getGid() : null;
	}

	@Override
	public Groups getRowData(String rowKey) {
	    @SuppressWarnings("unchecked")
		List<Groups> roleList = (List<Groups>) getWrappedData();
	    Integer value = Integer.valueOf(rowKey);

	    for (Groups role : roleList) {
	        if (role.getGid() == value) {
	            return role;
	        }
	    }

	    return null;
	}
	
	public Map<String,Object> getCurrentFilters() {
		return currentFilters;
	}
	
	public void setCurrentFilters(Map<String,Object> currentFilters) {
		this.currentFilters = currentFilters;
	}

}
