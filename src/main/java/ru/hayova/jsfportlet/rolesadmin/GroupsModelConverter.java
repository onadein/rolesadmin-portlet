package ru.hayova.jsfportlet.rolesadmin;

//(c) Oleg 'hayova' Nadein, 2016

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import ru.hayova.jsfportlet.rolesdb.Groups;

@FacesConverter(forClass = Groups.class,value="groupsModelConverter")
public class GroupsModelConverter implements Converter, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2184134264590673647L;

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
	    String str = "";
	    if (value instanceof Groups) {
	        str = "" + ((Groups) value).getGid();
	    }

	    return str;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
	    Object ret = null;
	    if (arg1 instanceof PickList) {
	        Object dualList = ((PickList) arg1).getValue();
	        DualListModel dl = (DualListModel) dualList;
	        for (Object o : dl.getSource()) {
	            String id = "" + ((Groups) o).getGid();
	            if (value.equals(id)) {
	                ret = o;
	                break;
	            }
	        }
	        if (ret == null)
	            for (Object o : dl.getTarget()) {
	                String id = "" + ((Groups) o).getGid();
	                if (value.equals(id)) {
	                    ret = o;
	                    break;
	                }
	            }
	    }
	    
	    return ret;
	}
	
}
