package ru.hayova.jsfportlet.rolesdb;

//(c) Oleg 'hayova' Nadein, 2016

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "commands", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class Commands implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer cid;
	private String name;
	private String description;
	private Set<Groups> groupses = new HashSet<Groups>(0);

	public Commands() {
	}

	public Commands(String name) {
		this.name = name;
	}

	public Commands(String name, String description, Set<Groups> groupses) {
		this.name = name;
		this.description = description;
		this.groupses = groupses;
	}

	@Id
	@Column(name = "cid", unique = true, nullable = false)
	public Integer getCid() {
		return this.cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "commandses")
	public Set<Groups> getGroupses() {
		return this.groupses;
	}

	public void setGroupses(Set<Groups> groupses) {
		this.groupses = groupses;
	}

}
