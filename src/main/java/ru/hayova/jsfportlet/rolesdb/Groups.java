package ru.hayova.jsfportlet.rolesdb;

//(c) Oleg 'hayova' Nadein, 2016

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "groups", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class Groups implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer gid;
	private String name;
	private String description;
	private Set<Roles> roleses = new HashSet<Roles>(0);
	private Set<Commands> commandses = new HashSet<Commands>(0);

	public Groups() {
	}

	public Groups(String name) {
		this.name = name;
	}

	public Groups(String name, String description, Set<Roles> roleses, Set<Commands> commandses) {
		this.name = name;
		this.description = description;
		this.roleses = roleses;
		this.commandses = commandses;
	}

	@Id
	@Column(name = "gid", unique = true, nullable = false)
	public Integer getGid() {
		return this.gid;
	}

	public void setGid(Integer gid) {
		this.gid = gid;
	}

	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "groupses")
	public Set<Roles> getRoleses() {
		return this.roleses;
	}

	public void setRoleses(Set<Roles> roleses) {
		this.roleses = roleses;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "groups_commands", schema = "public", joinColumns = {
			@JoinColumn(name = "gid", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "cid", nullable = false, updatable = false) })
	public Set<Commands> getCommandses() {
		return this.commandses;
	}

	public void setCommandses(Set<Commands> commandses) {
		this.commandses = commandses;
	}

}
