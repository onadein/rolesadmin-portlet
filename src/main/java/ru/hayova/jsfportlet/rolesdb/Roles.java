package ru.hayova.jsfportlet.rolesdb;

//(c) Oleg 'hayova' Nadein, 2016

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "roles", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class Roles implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer rid;
	private String name;
	private String description;
	private Date lastUpdate;
	private Set<Whitelist> whitelists = new HashSet<Whitelist>(0);
	private Set<Groups> groupses = new HashSet<Groups>(0);

	public Roles() {
	}

	public Roles(int rid) {
		this.rid = rid;
	} 
	
	public Roles(String name, String description, Set<Whitelist> whitelists, Set<Groups> groupses) {
		this.name = name;
		this.description = description;
		this.whitelists = whitelists;
		this.groupses = groupses;
	}

	@Id
	@Column(name = "rid", unique = true, nullable = false)
	public Integer getRid() {
		return this.rid;
	}

	public void setRid(Integer rid) {
		this.rid = rid;
	}

	@Column(name = "name", unique = true)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "roles")
	public Set<Whitelist> getWhitelists() {
		return this.whitelists;
	}

	public void setWhitelists(Set<Whitelist> whitelists) {
		this.whitelists = whitelists;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "roles_groups", schema = "public", joinColumns = {
			@JoinColumn(name = "rid", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "gid", nullable = false, updatable = false) })
	public Set<Groups> getGroupses() {
		return this.groupses;
	}

	public void setGroupses(Set<Groups> groupses) {
		this.groupses = groupses;
	}

	@Column(name = "last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
