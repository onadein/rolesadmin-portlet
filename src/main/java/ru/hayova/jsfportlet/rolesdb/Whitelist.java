package ru.hayova.jsfportlet.rolesdb;

//(c) Oleg 'hayova' Nadein, 2016

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "whitelist", schema = "public")
public class Whitelist implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Roles roles;
	private String netaddress;
	private String netname;
	private Date lastUpdate;

	public Whitelist() {
	}

	public Whitelist(Roles roles, String netaddress, String netname) {
		this.roles = roles;
		this.netaddress = netaddress;
		this.netname = netname;
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "netrole")
	public Roles getRoles() {
		return this.roles;
	}

	public void setRoles(Roles roles) {
		this.roles = roles;
	}

	@Column(name = "netaddress")
	public String getNetaddress() {
		return this.netaddress;
	}

	public void setNetaddress(String netaddress) {
		this.netaddress = netaddress;
	}

	@Column(name = "netname")
	public String getNetname() {
		return this.netname;
	}

	public void setNetname(String netname) {
		this.netname = netname;
	}

	@Column(name = "last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
