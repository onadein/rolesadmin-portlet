package ru.hayova.jsfportlet.errorhandler;

//(c) Oleg 'hayova' Nadein, 2016

import java.util.Iterator;
import java.util.Map;
 
import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

public class CustomExceptionHandler extends ExceptionHandlerWrapper {
  private ExceptionHandler wrapped;
 
 
  public CustomExceptionHandler(ExceptionHandler wrapped) {
    this.wrapped = wrapped;
  }
 
  @Override
  public ExceptionHandler getWrapped() {
    return wrapped;
  }
 
  @Override
  public void handle() throws FacesException {
    Iterator<ExceptionQueuedEvent> iterator = getUnhandledExceptionQueuedEvents().iterator();
 
    while (iterator.hasNext()) {
      ExceptionQueuedEvent event = (ExceptionQueuedEvent) iterator.next();
      ExceptionQueuedEventContext context = (ExceptionQueuedEventContext)event.getSource();
 
      Throwable throwable = context.getException();

      FacesContext fc = FacesContext.getCurrentInstance();
 
      try {
	      Map<String, Object> requestMap = fc.getExternalContext().getRequestMap();
	      
	      requestMap.put("errorDetails", throwable.getMessage());
	          
          if(!(throwable instanceof ViewExpiredException))
          		throwable.printStackTrace();
          
          NavigationHandler navigationHandler = fc.getApplication().getNavigationHandler();
 
          navigationHandler.handleNavigation(fc, null, "error?faces-redirect=false");
	      
	      fc.renderResponse();
          
	} finally {
          iterator.remove();
      }
    }    
    getWrapped().handle();
  }
  
}
