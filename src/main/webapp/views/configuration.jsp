<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>

<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>

<portlet:defineObjects />

<%
	String defaultUsername = GetterUtil.getString(portletPreferences.getValue("defaultUsername", "rolesdb"));
	String defaultPassword = GetterUtil.getString(portletPreferences.getValue("defaultPassword", "q1w2e3"));
	String defaultDBName = GetterUtil.getString(portletPreferences.getValue("defaultDBName", "rolesdb"));
	String defaultDBPath = GetterUtil.getString(portletPreferences.getValue("defaultDBPath", "localhost:5432"));
%>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationURL" />

<form action="<%= configurationURL %>" method="post" name="fm" >
	<input name="<portlet:namespace /><%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<div id="dbfields">
		<table> 
			<tr>
				<td>Database username: </td>
				<td><aui:input name="preferences--defaultUsername--" type="text" value="<%= defaultUsername %>" label="<%= StringPool.BLANK %>" style="margin-bottom:-22px;" /></td>
			</tr>
			<tr>
				<td>Database password: </td>
				<td><aui:input name="preferences--defaultPassword--" type="text" value="<%= defaultPassword %>" label="<%= StringPool.BLANK %>" style="margin-bottom:-22px;" /></td>
			</tr>
			<tr>
				<td>Database name: </td>
				<td><aui:input name="preferences--defaultDBName--" type="text" value="<%= defaultDBName %>" label="<%= StringPool.BLANK %>" style="margin-bottom:-22px;" /></td>
			</tr>
			<tr>
				<td>Database hostname (IP-address) and port: </td>
				<td><aui:input name="preferences--defaultDBPath--" type="text" value="<%= defaultDBPath %>" label="<%= StringPool.BLANK %>" style="margin-bottom:-22px;" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Save" /></td>
			</tr>
		</table>
	</div>
</form>